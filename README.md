# Engineering Technical Test 2 (Cart)
## Amaysim Shopping Cart Exercise

> $ git clone https://phillipfjimenez@bitbucket.org/phillipfjimenez/amaysim.git

Go to the directory where you run the command. then open cloned folder "amaysim".

1. Open Browser - Google Chrome
2. Drag the index.html file
3. Right Click on the White Page Screen
4. Click "Inspect Element" or CTRL + SHIFT + C for keyboard Shortcut
5. Select Console then follow the Steps/Commands in the Exam's Document

Example:

```sh
cart = ShoppingCart.new(pricingRules);
cart.add(item1);
cart.add(item2, promo_code);
cart.total;
cart.items;
```

Note: We have(item1, item2, item3, item4 and promo_code)

Refresh the Page to Reset the Program and to Test the other Scenario's and Repeat the Example using a different item.

This Exercise doesn't need session for the Cart?